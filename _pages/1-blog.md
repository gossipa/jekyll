---
layout: postlist
title: Blog

# Enable / Disable events Google Analytics for this link page.
ga_event: false

# Icon feature uses Font Awesome
icon: fa-edit

# Strings used for posts counter message
msg_qtd: ['Hay','entradas en total.']

# Enable / Disable 'Tag' and 'Search' buttons in the post listing layout.
buttons: false

# Enable / Disable this page in the main menu.
menu: true

published: true

# Does not change and does not remove 'script' variables
script: [postlist.js]

permalink: /blog/
---

