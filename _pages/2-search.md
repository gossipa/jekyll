---
layout: search
title: Búsqueda 

# Enable / Disable events Google Analytics for this link page.
ga_event: false

# Icon feature uses Font Awesome
icon: fa-search

# Enable / Disable this page in the main menu.
menu: true

published: true

# Does not change and does not remove 'script' variables
script: [search.js]

permalink: /blog/search/
---

Escriba algo en el campo para realizar una búsqueda de un artículo en el Blog. ¡Buena búsqueda!

