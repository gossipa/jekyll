---
layout: post
title:  "Jekyll y Github Pages"
date:   2017-09-08 19:08:10 +0200
language: es_ES 
categories: blog
published: true
comments: true 
excerpted: |
    Mi primer post y es acerca de este blog...

# Does not change and does not remove 'script' variables
script: [post.js]
---
Este es mi primer post en _Github Pages_ usando como herramienta de ayuda _Jekyll_ como creador de contenido estático. Después de trastear por Internet, y gracias a la ayuda de podcasts como el de (http://lormez16.github.io/experimentando-con-jekyll), al fin he creado este blog.

La idea que quiero llevar a cabo es la de seguir experiementando con Jekyll e ir mejorando el aspecto (o empeorando, nunca se sabe) de este sitio.

