---
layout: post
title:  "Archlinux y Antergos"
date:   2017-09-09 11:20:10 +0200
categories: blog
published: true
excerpted: |
    Mi (todavía) breve experiencia con Antergos y Archlinux...

# Does not change and does not remove 'script' variables
script: [post.js]
---
Quería hablar en esta entrada acerca de mi experiencia, todavía pequeña pues llevo apenas unas semanas con _Arch Linux_ y _Antergos_. ¿Qué son estos nombres? _Arch Linux_, (http://www.archlinux.org), es una distribución del sistema operativo _Linux_ o _GNU/Linux_, cuyo propósito es de ser ligera, flexible y que trata de mantener todo de forma simple, según reza en el enunciado de su página web. _Antergos_, (http://antergos.com),  es una distribución basada en Arch Linux cuyo propósito es tener siempre actualizado con lo último tu sistema, es lo que se conoce como una distribución "rolling release".

He instalado Antergos tanto en el portátil como en el _Mac mini_. Esto último me llevará otra entrada que ahora no tiene lugar. Antergos bśicamente es un Arch Linux con sus peculiaridades pero Arch Linux al fin y al cabo. De momento lo que he notado es que ciertamente es una distribución que efectivamente muy ligera, tarda poco en arrancar comparado con lo que lo hacía últimamente mi _Mac OSX Gran Capitan_, aunque probablemente se deba a la mayor cantidad de servicios y aplicaciones que estaban instalados en este último. E incluso creo que ligeramente más rápido que el _Windows 10_ instalado en la partición Bootcamp del Mac mini, aunque esto no es más que una apreciación, tengo que hacer pruebas al respecto.

Uso como escritorio _Gnome 3_ lo cual lo hace muy similar al uso con otras distribuciones. Me ha gustado el gestor de paquetes, lo veo muy sencillo y fácil de buscar, además con bastante software, pensaba que estaría más limitado que en _Ubuntu_ o _CentOS_ pero realmente encuentro muchas cosas que necesito. He instalado las aplciaciones que necesito y todo de momento ha ido muy bien. Las aplicaciones suelen cargar rápido y al ser Gnome, que ya lo conozco, puedo ir más rápido, en resumen, que he aumentado mi productividad en el _Mac Mini_.

Como pegas al uso de Antergso/arch Linux, las he encontrado en el uso de la impresora. Tengo una _Canon Pixma MG 5750_ y la verdad es que aunque la detecta el sistema, no he conseguido hacerla funcionar. He activado el servicio _LPR_ en la imrpesora, he probado varias configuaciones, desde las aplciaciones del sistemas al propio servio de _CUPS_ pero no ha sido posible. Una opción que he encontrado, más allá de usar una máquina virtual con Windows 10, es uar otra distribución de Linux y con eso pues también aprovecho para probar otra y otro escritorio. En este caso decidí usar _Linux Mint_, una distribución con mucha popularidad, basada en Ubuntu pero con personalidad propia, y número 1 en la página de _Distrowatch_, http://distrowatch.com, que mantienen un ranking de popularidad de distribuciones _GNU/Linux_ (y otros sistemas operativos Open Source) basado en las visitas a cada una de ellas dentro de Distrowatch. _Linux Mint_ con _Cinnamon_ com escritorio, que he de reconocer que es también muy interesante, detectó sin probleams la impresora. COn esto tengo el problema resuelto pero bueno estoy seguro que taarde o temprano volveré a intentar configura la impresora.

En otros casos me he encontrado con que hay alguna aplicación que no viene dentro de la paquetería del sistema. En los casos de paquetes DEB o RPM suele haber mucho disponible, y la verdad es que facilitan la instalación de cualquier aplicación, no siempre una aplicación en formato "tar.gz", es decir, mediante los fuentes ayuda a instalar la aplicación.

La conclusión es que estoy satisfecho con el cambio. Me ha costado un poco el poder usar una distibución Linux ya que Apple no te lo pone fácil pero al final ya puedo escribir estas líneas desde Antergos con el Mac mini.

