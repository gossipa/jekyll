---
layout: post
title:  "Canon Pixma MG5750 in Archlinux"
date:   2017-09-11 21:10:10 +0200
categories: blog
published: true
comments: true 
excerpted: |
    Canon MG Pixma 5750 install on Arch Linux.

# Does not change and does not remove 'script' variables
script: [post.js]
---

How to install Canon MG Pixma 5750 (or other models) on your Arch Linux (or derivatives).

This tutorial is written to those who has a Canon Pixma 5750 (or another similar Canon model) via Network and wants to use it in their ArchLinux system. I have tested it on a Antergos distribution but I think it will works on another distribution similar to Antergos like Archlinux or Manjaro.

##### The printer

* Activate LPR service. Inside the printer we have to active LPR Service:

<pre> Settings / Device settings / LAN settings / LPR protocol setting / Enable </pre>

* Take the address of your printer:

If the case that you you use a network printer you will need an ip address. One way to get the ip address if you don't know it is to print it through the printer:

<pre> Settings / Device settings / LAN settings / Confirm LAN Settings / Print LAN Settings </pre>

* Packages:

Printer needs some package, in particular, _cnijfilter2-mg7700_. Through the package manager, search it and install it:
:
* Add printer to system

Go to _Printer settings_:

<pre> Unlock / Add / LPD/LPR / Machine: ip-address-of-you-printer / Provide PPD file </pre>

The printer PPD file path which contains the printer drivers is:

<pre> /usr/share/ppd/canonmg5700.ppd </pre>


##### The scanner

* Add some packages:

Use Package Manager and install this tools:

_sane_

_xsane_

* Edit /etc/sane.d/pixma.conf:

<pre> /etc/sane.d/pixma.conf </pre>

Add the printer ipaddress through "bjnp" protocol

<pre> bjnp://your-printer-ip-address </pre>

* Last through _xsane_ we can use the scanner.
